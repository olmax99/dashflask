FROM python:3.7


RUN pip install meinheld gunicorn

COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

COPY ./gunicorn_conf.py /gunicorn_conf.py


ENV PORT 5000
ENV BIND "0.0.0.0:5000"

# Change log level of Gunicorn: debug, info, warning, error, critical (default: info)
# ENV LOG_LEVEL="debug"

COPY ./ /app

WORKDIR /app

RUN pip install pipenv
RUN ls -al && pipenv install --system --deploy --ignore-pipfile


ENTRYPOINT ["/entrypoint.sh"]
